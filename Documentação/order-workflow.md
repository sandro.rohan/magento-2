# Introdução

O objetivo desse documento é detalhar o workflow da geração de um pedido, desde a sua criação até a sua finalização.

Para isso, reserva-se:
1. Descobrir todas as etapas de um pedido dentro do Magento;
2. Verificar a possibilidade de criar dentro do Magento um campo que registra o histórico de alterações do pedido.

## Como funcionam as etapas de um pedido dentro do magento?

### Fonte(s): 
- https://www.magenteiro.com/blog/magento-1/3-coisas-que-voce-precisa-saber-para-entender-como-os-status-de-pedidos-funcionam-no-magento/
- https://docs.mageshop.com.br/books/gerenciar-suas-vendas-e-clientes---magento-1/page/processo-de-venda-magento-status-e-estado---magento-1

### As etapas de um pedido dentro do Magento estão divididas em `Estados` e `Status`. 

O termo `Estado` se refere a uma fase do fluxo de compra. Um Estado pode ter vários Status.

Estados identificados:

1. Novo (new)
2. Pagamento Pendente (pending payment)
3. Em espera (on hold)
4. Cancelado (canceled)
5. Processando (processing)
6. Completo (complete)
7. Fechado (closed)

O termo "Status" se refere a um status dentro do Estado. É possível criar novos status e alocá-los dentro de Estados.
Status identificados:
- Ação
- Pagamento confirmado
- Reembolso
- Fatura do pedido
- Encomenda enviada
- Pedido arquivado

O que o cliente enxerga é o Status do pedido, desta forma você pode ter um status “Em Separação” e “Pagamento Confirmado” dentro do mesmo Estado de pedido (Processing), já que os dois status só deveriam ocorrer quando o pagamento fosse confirmado.

## Fluxo de pedido (Em acordo com a documentação do Magento 2)
### Fonte(s): 
- https://docs.magento.com/user-guide/sales/order-workflow.html

### Pedido realizado (Place Order)

Esse processo ocorre quando o cliente realiza o pedido.

### Pedido pendente (Order Pending)

Na grid de pedidos, o Estado do pedido é inicialmente Pending. O pagamento ainda não foi processado, e o pedido ainda pode ser editado ou cancelado.

### Pagamento recebido (Receive Payment)

O Estado do pedido é alterado para Processing quando o pagamento é recebido ou autorizado.

### Fatura do pedido (Invoice Order)

Um pedido é tipicamente faturado quando o pagamento é recebido. Algumas formas de pagamento geram uma fatura automaticamente quando o pagamento é autorizado e capturado. 

### Reserva de uma remessa única

Quando o detalhamento do envio é completo, o envio é reservado. O cliente recebe uma notificação, e o pacote é enviado. Se números de rastreamento forem utilizados, o envio pode ser rastreado através da área do cliente.

## Processamento de um pedido
### Fonte(s): 
- https://docs.magento.com/user-guide/sales/order-processing.html
- https://docs.magento.com/user-guide/sales/order-ship.html

### Passos
1. Uma ordem em estado Pending pode ser modificada, colocada em espera, cancelada, faturada ou enviada.

2. Uma ordem em estado Processing não pode mais editar o conteúdo ou cancelar o pedido, mas o endereço de cobrança e envio pode ser editado.

3. Uma ordem em estado Completed pode ser reordenada.

## Fluxo de status do pedido
### Fonte(s):
- https://docs.magento.com/user-guide/sales/order-update.html
- https://docs.magento.com/user-guide/sales/order-status.html

### Imagem ilustrativa: 

![Workflow](/assets/order-workflow.png)

## Como criar status customizados e atribui-los a Estados
### Fonte(s):
- https://docs.magento.com/user-guide/sales/order-status-custom.html

`Nota:` Somente os valores de status de pedido personalizado padrão são usados ​​no fluxo de trabalho do pedido. Os valores de status personalizados que não são definidos como padrão podem ser usados ​​apenas na seção de comentários do pedido.

## Conclusão

Conclui-se que ambas as demandas foram sanadas, haja visto que:

1. As etapas de um pedido dentro do Magento foram dissecadas.
2. Dentro do Magento 2 já existe uma seção de comentários do pedido onde é possível verificar o histórico de alterações no pedido.